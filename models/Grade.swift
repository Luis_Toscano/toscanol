//
//  Grade.swift
//  examen1erbimestre
//
//  Created by LuisT on 5/6/18.
//  Copyright © 2018 LuisT. All rights reserved.
//

import Foundation

struct Grade:Decodable {
    let label: String;
    let value:Int;
    
}
