//
//  Header.swift
//  examen1erbimestre
//
//  Created by LuisT on 5/6/18.
//  Copyright © 2018 LuisT. All rights reserved.
//

import Foundation

struct Header:Decodable {
    let viewTitle:String;
    let date:String;
    let nextLink:String;
}
