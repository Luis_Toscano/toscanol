//
//  DetailsViewController.swift
//  examen1erbimestre
//
//  Created by LuisT on 5/6/18.
//  Copyright © 2018 LuisT. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var averageLabel: UILabel!
    
    var headerInfo:(name: String, nextUrl: String)?
    
    @IBOutlet weak var n1Label: UILabel!
    @IBOutlet weak var grade1Label: UILabel!
    
    @IBOutlet weak var n2Label: UILabel!
    @IBOutlet weak var grade2Label: UILabel!
    
    
    @IBOutlet weak var n3Labe: UILabel!
    @IBOutlet weak var grade3Label: UILabel!
    
    @IBOutlet weak var n4Label: UILabel!
    @IBOutlet weak var grade4Label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        nameLabel.text = headerInfo?.name;
        
        let URLendpoint:URL = URL(string: (headerInfo?.nextUrl)!)!;
        let session:URLSession = URLSession.shared;
        
        let task:URLSessionTask = session.dataTask(with: URLendpoint) { (data, response, error) in
            guard let data = data else {
                print("Error NO data")
                return;
            }
            
            guard let deserializedDetail = try? JSONDecoder().decode([Grade].self, from: data) else {
                print("Error decoding Detail")
                return
            }
            
            DispatchQueue.main.async {
                self.n1Label.text = deserializedDetail[0].label;
                self.n2Label.text = deserializedDetail[1].label;
                self.n3Labe.text = deserializedDetail[2].label;
                self.n4Label.text = deserializedDetail[3].label;
                
                self.grade1Label.text =  "\(deserializedDetail[0].value)";
                self.grade2Label.text = "\(deserializedDetail[1].value)";
                self.grade3Label.text = "\(deserializedDetail[2].value)";
                self.grade4Label.text = "\(deserializedDetail[3].value)";
                let average = (deserializedDetail[0].value + deserializedDetail[1].value + deserializedDetail[2].value + deserializedDetail[3].value) / 4;
                
                self.averageLabel.text = "\(average)";
            }
        }
        
        task.resume();
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true);

    }
    
    
}
