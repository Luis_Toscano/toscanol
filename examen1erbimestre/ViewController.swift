//
//  ViewController.swift
//  examen1erbimestre
//
//  Created by LuisT on 5/6/18.
//  Copyright © 2018 LuisT. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var viewTitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    
    var secondEndpoint:String = "";
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! DetailsViewController
        destination.headerInfo = (nameTextField.text, secondEndpoint) as? (name: String, nextUrl: String);
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let firstEndpoint:String = "https://api.myjson.com/bins/72936";
        let URLendpoint:URL = URL(string: firstEndpoint)!;
        let session:URLSession = URLSession.shared;
        
        let task:URLSessionTask = session.dataTask(with: URLendpoint) { (data, response, error) in
            guard let data = data else {
                print("Error NO data")
                return;
            }
            
            guard let deserializedHeader = try? JSONDecoder().decode(Header.self, from: data) else {
                print("Error decoding Header")
                return
            }
            
            DispatchQueue.main.async {
                self.viewTitleLabel.text = deserializedHeader.viewTitle;
                self.dateLabel.text = deserializedHeader.date;
                self.secondEndpoint = deserializedHeader.nextLink;
            }
        }
        
        task.resume();
    }

    
    @IBAction func nextPressed(_ sender: Any) {
        performSegue(withIdentifier: "toDetailSegue", sender: self);
        
    }
    
}

